# react-gallery


A simple gallery created with React. It uses Google Firebase for data storage and authentication.


### Installation

Create src/config.js file and export there your firebase api key and database url:

src/config.js:

```sh
export const API_KEY = 'your_google_firebase_api_key';
export const DB_LINK = 'https://your_project_name.firebaseio.com/';
```

Install the dependencies and devDependencies and start the server

```sh
$ npm install
$ npm run start
$ npm run watch-css
```
