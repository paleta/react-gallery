import React from 'react';
import Aux from '../_Aux/_Aux';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import { connect } from 'react-redux';

class Layout extends React.Component {

    state = {
        mobileMenu: false
    };

    onTriggerMobileMenu = () => {
        this.setState((prevState) => {
            return { mobileMenu: !prevState.mobileMenu };
        });
    };

    render() {  

        return (
            <Aux>
                <Header 
                        onTriggerMobileMenu={this.onTriggerMobileMenu}
                        showMobileMenu={this.state.mobileMenu}
                        location={this.props.location}
                        isAuthenticated={this.props.isAuthenticated}
                        userName={this.props.userName}/>
                <main>
                    <div className="container">
                        {this.props.children}
                    </div>               
                </main>
                <Footer isAuthenticated={this.props.isAuthenticated}/>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null,
        userName: state.auth.userName
    };
};

export default connect( mapStateToProps )( Layout );