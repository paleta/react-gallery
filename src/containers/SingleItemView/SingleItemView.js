import React from 'react';
import { connect } from 'react-redux';

import './SingleItemView.css';
import * as actions from '../../store/actions';
import Loader from '../../components/UI/Loader/Loader';
import Button from '../../components/UI/Button/Button';
import Icons from '../../assets/icons/sprite.svg';
import NotFound from '../../components/UI/NotFound/NotFound';
import { isEmpty } from '../../shared/utilities';


class SingleItemView extends React.Component {

    componentDidMount() {

        if (!this.props.galleryItem) {
            this.props.onGalleryGetItem(null, this.props.match.params.id);
        } 
    }

    onReturnBtnHandler = () => {
        this.props.history.push('/gallery');
    };


    render () {
        let contentToRender = (this.props.isBusyWithGetOne || !this.props.galleryItem) ? <Loader loaderClasses="single-item-view__loader"/> : 
        (
            <div className="single-item-view__content">
                <Button classes="single-item-view__btn" clicked={this.onReturnBtnHandler}>
                    <svg className="single-item-view__icon">
                        <use xlinkHref={`${Icons}#icon-keyboard_return`}/>
                    </svg>
                </Button>
                {(!!this.props.galleryItem && !isEmpty(this.props.galleryItem)) ? ( 
            
                <img 
                    className="single-item-view__image"
                    src={this.props.galleryItem.src} 
                    alt={this.props.galleryItem.title}
                    title={this.props.galleryItem.title}/>
                ) : <NotFound>This picture does NOT exist!</NotFound> }
            </div>
        );

        return (
            <section className="single-item-view">
                {contentToRender}
            </section>
        );
    };
};

const mapStateToProps = state => {
    return {
        isBusyWithGetOne: state.gallery.isBusyWithGetOne,
        galleryItem: state.gallery.galleryItem,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGalleryGetItem: (galleryItem, galleryItemId) => dispatch(actions.galleryGetItem(galleryItem, galleryItemId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SingleItemView);
