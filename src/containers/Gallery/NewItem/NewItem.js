import React from 'react';

import './NewItem.css';
import { createInput, validate } from '../../../shared/utilities';
import Button from '../../../components/UI/Button/Button';
import Input from '../../../components/UI/Input/Input';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions';
import Loader from '../../../components/UI/Loader/Loader';

class NewItem extends React.Component {

    state = {
        inputs: {
            title: {
                ...createInput('text', 'title', 'Please enter a title', {
                    required: true,
                    minLength: 2,
                    maxLength: 30,
                })
            },
            body: {
                ...createInput('textarea', 'body', 'Please enter short description', {
                    required: true,
                    minLength: 2,
                    maxLength: 138,
                })
            },
            link: {
                ...createInput('text', 'link', 'Please enter picture url', {
                    required: true,
                    minLength: 2,
                })
            },
        }
    };

    
    onInputChanged = (inputName, event) => {

        const input = {
            ...this.state.inputs[inputName],
            value: event.target.value,
            valid: validate(event.target.value, this.state.inputs[inputName].validation),
            touched: true
        };

        this.setState({
            inputs: {
                ...this.state.inputs,
                [inputName]: input
            }
        }); 
    };

    onSubmitHandler = (event) => {     
        event.preventDefault();
        this.props.onAddItem({
            title: this.state.inputs.title.value,
            src: this.state.inputs.link.value, 
            body: this.state.inputs.body.value,
            positiveVotes: 0,
            negativeVotes: 0,
        }, this.props.token);
    };

    render () {

        const formElementsArray = [];
        let areAllInputsValid = true;
        for (const inputName in this.state.inputs) {
            if (this.state.inputs.hasOwnProperty(inputName)) {
                formElementsArray.push(this.state.inputs[inputName]); 
                areAllInputsValid = this.state.inputs[inputName].valid && areAllInputsValid;           
            }
        }

        const formElements = formElementsArray.map((input, idx) => {
            return (
                <Input 
                    type={input.type}
                    name={input.name}
                    key={idx}
                    invalid={!input.valid}
                    touched={input.touched}
                    placeholder={input.placeholder}
                    value={input.value}
                    changed={this.onInputChanged.bind(this, input.name)}/>
            );
        });

        let contentToRender = null;

        if (this.props.show) {

            
            contentToRender = this.props.isBusyWithAdd ? <Loader loaderContainerClasses="new-item__loader"/> : (
                <section className="new-item">
                    <form onSubmit={this.onSubmitHandler} className="form">
                        {formElements}
                        <Button 
                            type="submit"
                            disabled={!areAllInputsValid}
                            classes="form__btn">Add</Button>
                    </form>
                </section>
            );


        } 

        return contentToRender;
    };
};


const mapStateToProps = state => {
    return {
        isBusyWithAdd: state.gallery.isBusyWithAdd,
        token: state.auth.token
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddItem: (newItem, token) => dispatch(actions.galleryAddItem(newItem, token))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewItem);