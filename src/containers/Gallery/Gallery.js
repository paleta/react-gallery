import React from 'react';
import { connect } from 'react-redux';
import ReactTimeout from 'react-timeout'

import './Gallery.css';
import GalleryItem from '../../components/GalleryItem/GalleryItem';
import Button from '../../components/UI/Button/Button';
import NewItem from './NewItem/NewItem';
import * as actions from '../../store/actions';
import Loader from '../../components/UI/Loader/Loader';
import Aux from '../../hoc/_Aux/_Aux';
import Modal from '../../components/UI/Modal/Modal';
import NotFound from '../../components/UI/NotFound/NotFound';


class Gallery extends React.Component {

    state = {
        showModal: false,

        startDeleting: false,
        selectedItem: {
            id: null,
            index: null
        }
    }

    componentDidMount () {
        this.props.onGalleryGetItems();
    }

    onButtonClickedHandler = () => {
        this.props.history.push('/gallery/add-new-picture');
    }

    onDeleteSingleItemHandler = () => {


        this.setState({
            showModal: false,        
        });

        if (this.errorTimeout) {
            clearInterval(this.errorTimeout)
        } 

        this.errorTimeout = this.props.setTimeout(() => {
            this.props.onGalleryDeleteItem(this.props.token, this.selectedItem.id, this.selectedItem.index);;
        }, 200);

    }

    onRequireConfirmationHandler = (id, index) => {
        this.setState({
            showModal: true,
        })
        this.selectedItem = {
            id: id,
            index: index
        };

    }

    onDeclineHandler = () => {
        this.setState({     
            showModal: false,      
        });
        this.selectedItem = {
            id: null,
            index: null
        };
    }
   
    onThumbHanlder = (updatedItem, itemIndex, mode) => {

        this.itemToUpdateId = updatedItem.id;

        let itemToUpdate = {
            ...updatedItem
        }

        delete itemToUpdate.isNew;

        if (mode === 'UP') {
            itemToUpdate.positiveVotes = (+itemToUpdate.positiveVotes + 1);
        } else {
            itemToUpdate.negativeVotes = (+itemToUpdate.negativeVotes + 1); 
        }

        this.props.onGalleryUpdateItem(this.props.token, itemToUpdate, itemIndex);
    }

    onOpenSingleItemHandler = galleryItem => {
        this.props.onGalleryGetItem(galleryItem, null);
        this.props.history.push(`/gallery/${galleryItem.id}`)
    }

    render() {

        let galleryItems = (this.props.galleryItems && this.props.galleryItems.length !== 0) ?       
            this.props.galleryItems.map((galleryItem, idx) => {

                const isNew = !!galleryItem.isNew;
                const toDelete = this.props.isBusyWithDelete && this.selectedItem.id === galleryItem.id;
                const toUpdate = this.props.isBusyWithUpdate && this.itemToUpdateId === galleryItem.id;

                return (
                    <GalleryItem 
                        key={galleryItem.id}
                        isNew={isNew}
                        toDelete={toDelete}
                        path={galleryItem.src} 
                        title={galleryItem.title} 
                        body={galleryItem.body}
                        toUpdate={toUpdate}
                        onOpen={this.onOpenSingleItemHandler.bind(this, galleryItem)}
                        onThumbUp={this.onThumbHanlder.bind(this, galleryItem, idx, 'UP')}
                        onThumbDown={this.onThumbHanlder.bind(this, galleryItem, idx, 'DOWN')}
                        positiveVotes={galleryItem.positiveVotes}
                        negativeVotes={galleryItem.negativeVotes}
                        onRequireConfirmation={this.onRequireConfirmationHandler.bind(this, galleryItem.id, idx)}/>
                );
            }) : <NotFound>Nothing found. Add something cool!</NotFound>;
       

        let showForm = this.props.location.pathname.indexOf('/gallery/add-new-picture') !== -1;

        const modal = this.state.showModal ? 
            (<Modal
                icon
                title="Are you sure"
                onConfirm={this.onDeleteSingleItemHandler}
                onDecline={this.onDeclineHandler}>Do you want to delete pernamently this picture?
            </Modal>) : null;

        let contentToRender = (!this.props.isBusyWithGetAll) ? (
            <Aux>
                <div className="gallery__controls">
                    <Button clicked={this.onButtonClickedHandler} classes="gallery__btn">+</Button>
                </div>
                <NewItem show={showForm}/>
                <div className="gallery__content">
                    {galleryItems} 
                </div>
                {modal}
            </Aux>
        ) : <Loader loaderContainerClasses="gallery__loader" />;

        return (
            <section className="gallery">
                {contentToRender}
            </section>                      
        );
    }
}


const mapStateToProps = state => {
    return {
        galleryItems: state.gallery.galleryItems,
        isBusyWithGetAll: state.gallery.isBusyWithGetAll,
        isBusyWithDelete: state.gallery.isBusyWithDelete,
        isBusyWithUpdate: state.gallery.isBusyWithUpdate,
        token: state.auth.token,
        isAuthenticated: state.auth.token !== null,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGalleryGetItems: () => dispatch(actions.galleryGetItems()),
        onGalleryDeleteItem: (token, itemId, itemIndex) => dispatch(actions.galleryDeleteItem(token, itemId, itemIndex)),
        onGalleryUpdateItem: (token, updatedItem, itemIndex) => dispatch(actions.galleryUpdateItem(token, updatedItem, itemIndex)),
        onGalleryGetItem: (galleryItem, galleryItemId) => dispatch(actions.galleryGetItem(galleryItem, galleryItemId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReactTimeout(Gallery));