import React from 'react';

import './SignIn.css';
import Input from '../../components/UI/Input/Input';
import { createInput, validate } from '../../shared/utilities';
import Button from '../../components/UI/Button/Button';
import * as actions from '../../store/actions';
import { connect } from 'react-redux';
import Loader from '../../components/UI/Loader/Loader';

class SignIn extends React.Component {

    state = {
        inputs: {
            email: {
                ...createInput('email', 'email', 'Please enter your email', {
                    required: true,
                    minLength: 2,
                    maxLength: 30,
                    isEmail: true
                })
            },
            password: {
                ...createInput('password', 'password', 'Please enter your password', {
                    required: true,
                    minLength: 6,
                    maxLength: 30,
                })
            }
        }
    };

    onInputChanged = (inputName, event) => {

        const input = {
            ...this.state.inputs[inputName],
            value: event.target.value,
            valid: validate(event.target.value, this.state.inputs[inputName].validation),
            touched: true
        };

        this.setState({
            inputs: {
                ...this.state.inputs,
                [inputName]: input
            }
        }); 


    };

    onSubmitHandler = (event) => {
        event.preventDefault();
        this.props.onAuth( this.state.inputs.email.value, this.state.inputs.password.value );
    };

    render() {

        const formElementsArray = [];
        let areAllInputsValid = true;

        for (const inputName in this.state.inputs) {
            if (this.state.inputs.hasOwnProperty(inputName)) {
                formElementsArray.push(this.state.inputs[inputName]); 
                areAllInputsValid = this.state.inputs[inputName].valid && areAllInputsValid;       
            }
        }

        const formElements = formElementsArray.map((input, idx) => {
            return (
                <Input 
                    type={input.type}
                    name={input.name}
                    key={idx}
                    invalid={!input.valid}
                    touched={input.touched}
                    placeholder={input.placeholder}
                    value={input.value}
                    changed={this.onInputChanged.bind(this, input.name)}/>
            );
        });

        let contentToRender = (
            <div className="sign-in__content">
            <h2 className="heading-secondary u-margin-bottom-medium">Sign in!</h2>
            <form onSubmit={this.onSubmitHandler} className="form">
                {formElements}
                <Button 
                    type="submit" 
                    disabled={!areAllInputsValid}
                    classes="form__btn">Submit</Button>                
            </form>
        </div>
        );

        if (this.props.busy) {
            contentToRender = <Loader loaderContainerClasses="sign-in__loader" />
        }


        return (
            <section className="sign-in">
                {contentToRender}
            </section>
        );
    };
}

const mapStateToProps = state => {
    return {
        busy: state.auth.busy,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
    };
};

const mapDispatchToProps = dispatch => {
    return { 
        onAuth: (email, password) => dispatch(actions.auth(email, password)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);