import React from 'react';
import './Welcome.css';
import Button from '../../components/UI/Button/Button';

class Welcome extends React.Component {


    onButtonClickedHandler = () => {
        // console.log(this.props);
        this.props.history.push('/gallery');
    };

    render() {
        return (
            <section className="welcome">
                <h1 className="heading-primary u-margin-bottom-medium">
                    <span className="heading-primary--main">Make yourself at home</span>
                    <span className="heading-primary--sub">Check out our awesome gallery!</span>
                </h1>
                <Button type="button" clicked={this.onButtonClickedHandler}>Check it out!</Button>
            </section>            
        );
    }
}

export default Welcome;