import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import ReactTimeout from 'react-timeout'


import Layout from './hoc/Layout/Layout';
import Welcome from './containers/Welcome/Welcome';
import Gallery from './containers/Gallery/Gallery';
import SignUp from './containers/SignUp/SignUp';
import SignIn from './containers/SignIn/SignIn';
import * as actions from './store/actions';
import { connect } from 'react-redux';
import Logout from './containers/Logout/Logout';
import Snackbar from './components/UI/Snackbar/Snackbar';
import SingleItemView from './containers/SingleItemView/SingleItemView';


class App extends Component {

  componentDidMount() {
    this.props.onCheckIfUserIsAuthenticated();
  }

  onError = () => {

    if (this.errorTimeout) {
      clearInterval(this.errorTimeout)
    } 
    
    this.errorTimeout = this.props.setTimeout(() => {
      this.props.onClearAuthError();
      this.props.onClearGalleryError()
    }, 5000);
  };

  render() {

    let currentLocation = !!this.props.galleryItem ? this.props.galleryItem.title : null;

    switch (this.props.location.pathname) {
      case '/':
        currentLocation = 'Home';
        break;
      case '/gallery':
      case '/gallery/':
        currentLocation = 'Gallery';
        break;
      case '/gallery/add-new-picture':
      case '/gallery/add-new-picture/':
        currentLocation = 'Gallery - add new picture!';
        break;
      case '/sign-up':
      case '/sign-up/':
        currentLocation = 'Sign up';
        break;
      case '/sign-in':
      case '/sign-in/':
        currentLocation = 'Sign in';
        break;
      default:
        break;
    }

    // console.log(this.props.location);

    let routes = this.props.isAuthenticated ? (
      <Switch>
        <Route path="/" exact component={Welcome}/>
        <Route path="/logout" exact component={Logout}/>
        <Route path="/gallery" exact component={Gallery}/>
        <Route path="/gallery/add-new-picture" exact component={Gallery}/>        
        <Route path="/gallery/:id" exact component={SingleItemView}/>
        <Redirect to="/" />
      </Switch>
    ) : (
      <Switch>
        <Route path="/" exact component={Welcome}/>
        <Route path="/sign-up" exact component={SignUp}/>
        <Route path="/sign-in" exact component={SignIn}/>
        <Route path="/gallery" exact component={Gallery}/>
        <Route path="/gallery/add-new-picture" exact component={Gallery}/>
        <Route path="/gallery/:id" exact component={SingleItemView}/>
        <Redirect to="/" />
      </Switch>
    );

    let snackbar = null;

    if (!!this.props.error) {
      snackbar = (
        <Snackbar>{this.props.error}</Snackbar>
      );
      this.onError();
    }

    return (
      <Layout location={currentLocation}>
        {routes}
        {snackbar}
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error || state.gallery.error,
    isAuthenticated: state.auth.token !== null,
    //galleryItem: !!state.gallery.galleryItem ? state.gallery.galleryItem : null
    galleryItem: state.gallery.galleryItem
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCheckIfUserIsAuthenticated: () => dispatch(actions.authCheckState()),
    onClearAuthError: () => dispatch(actions.authClearError()),
    onClearGalleryError: () => dispatch(actions.galleryClearError()),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ReactTimeout(App)));
