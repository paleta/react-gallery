export const createInput = (type, name, placeholder, validation) => {
    return {
        type: type,
        placeholder: placeholder,
        name: name,
        value: '',
        validation: {
            ...validation
        },
        valid: false,
        touched: false
    };
};

export const validate = (value, rules) => {
    let valid = true;


    if (rules.required) {
        valid = value.trim() !== '' && valid;;
    }
    if (rules.minLength) {
        valid = value.length >= rules.minLength && valid;
    }
    if (rules.maxLength) {
        valid = value.length <= rules.maxLength && valid;
    }
    if (rules.isEmail) {
        const pattern = /(.+)@(.+){2,}\.(.+){2,}/;
        valid = !!pattern.test(value) && valid;
    }

    // console.log(valid);

    return valid;
}

export const errorFormater = (err) => {
    if (err && err.response && err.response.data && err.response.data.error && err.response.data.error.message) {
        return err.response.data.error.message;
    } else if (err && err.response && err.response.data && err.response.data.error) {     
        return `Error: ${err.response.status} ${err.response.statusText} - ${err.response.data.error}`;
    } else if (err && err.response && err.response.data) {     
        return `Error: ${err.response.status} ${err.response.statusText} - ${err.response.data}`;
    } else {
        // console.log(JSON.stringify(err, null, 4));
        return 'UNKNOWN_ERROR';
    }
};

export const isEmpty = (obj) => {
    for(const key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
};