import React from 'react';
import './Footer.css';
import Logo from '../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import { Link } from 'react-router-dom';
import Panel from '../Panel/Panel';

const header = (props) => {

    return (
        <footer className="footer">
      
            <div className="footer__content">
                <NavigationItems isAuthenticated={props.isAuthenticated}
                                 classes="footer__nav"/>
                <Link to="/" className="footer__link">
                    <Logo classes="logo-container--big"/>
                </Link>
            </div>
       
            <Panel>
                <div className="info-block">Site created for fun with React 16.4.1 by <a className="footer__link" rel="noopener noreferrer" target="_blank" href="https://www.piosik.xyz">Patryk Piosik</a></div>
                <div>Copyright &copy;{new Date().getFullYear()}</div>
            </Panel>            
        </footer>
    );
};

export default header;