import React from 'react';
import './Panel.css';

const panel = (props) => {


    return (
        <div className="panel">
            {props.children}
        </div>
    );
};

export default panel;