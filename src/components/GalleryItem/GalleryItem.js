import React from 'react';

import Button from '../UI/Button/Button';
import './GalleryItem.css';
import ItemRating from './ItemRating/ItemRating';
import Icons from '../../assets/icons/sprite.svg';
import ItemControls from './ItemControls/ItemControls';
import Aux from '../../hoc/_Aux/_Aux';
import Loader from '../UI/Loader/Loader';
import Image from '../UI/Image/Image';

const galleryItem = (props) => {

    let animation = (props.isNew && !props.toDelete) ? 'showUpNewGalleryItem .5s ease' : 'none';

    if (props.toDelete) {
        animation = 'onDeleteItem .2s ease-out forwards';
    }

    let contentToRender = props.toUpdate ? <Loader loaderClasses="gallery-item__loader"/> : ( 
        <Aux>
            <h3 className="gallery-item__title heading-tertiary u-margin-bottom-low">{props.title}</h3>
            <p className="gallery-item__body paragraph u-margin-bottom-low">{props.body}</p>
            <div className="fill-remaning-space"></div>
            <Button clicked={props.onOpen} classes="gallery-item__btn button--hover-white u-margin-bottom-low">
                <span className="gallery-item__btn--text">Open</span>
                <svg className="gallery-item__btn--icon">
                    <use xlinkHref={`${Icons}#icon-link`} />
                </svg>
            </Button>
            <ItemRating 
                positiveVotes={props.positiveVotes} 
                negativeVotes={props.negativeVotes}/>
            <ItemControls 
                onThumbUp={props.onThumbUp}
                onThumbDown={props.onThumbDown}
                onRequireConfirmation={props.onRequireConfirmation}/>
        </Aux>
    );
    
    return (
        <figure className="gallery-item" style={{animation: animation}}>
            <div className="gallery-item__side gallery-item__side--front">
                {/* <img src={props.path} className="gallery-item__image" title={props.title} alt={props.title}/> */}
                <Image src={props.path} classes='gallery-item__image' title={props.title} alt={props.title}/>
            </div>
            <div className="gallery-item__side gallery-item__side--back">
                {contentToRender}
            </div>
        </figure>
    );
};

export default galleryItem; 