import React from 'react';

import './ItemControls.css';
import Icons from '../../../assets/icons/sprite.svg';

const itemControls = (props) => {
    return (
        <div className="controls">
            <svg className="control control--up" onClick={props.onThumbUp}>
                <use xlinkHref={`${Icons}#icon-thumbs-up`} />
            </svg>
            <svg className="control control--down" onClick={props.onThumbDown}>
                <use xlinkHref={`${Icons}#icon-thumbs-down`} />
            </svg>
            <div className="fill-remaning-space"></div>
            <svg className="control control--bin" onClick={props.onRequireConfirmation}>
                <use xlinkHref={`${Icons}#icon-bin`} />
            </svg>
        </div>
    );
};
export default itemControls;