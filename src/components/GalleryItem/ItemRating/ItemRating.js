import React from 'react';

import './ItemRating.css';
import Icons from '../../../assets/icons/sprite.svg';

const itemRating = (props) => {

    const sum = props.positiveVotes + props.negativeVotes;
    const precentage = sum > 0 ? (props.positiveVotes * 100)  / sum  : 0;

    let starsLength = null;

    if (precentage < 5) {
        starsLength = [0, 0, 0, 0, 0]; // 0 - full stars, 0 - half stars, 5 empty stars
    } else if (precentage >= 5 && precentage < 15) {
        starsLength = [.5, 0, 0, 0, 0];
    } else if (precentage >= 15 && precentage < 25) {
        starsLength = [1, 0, 0, 0, 0];
    } else if (precentage >= 25 && precentage < 35) {
        starsLength = [1, .5, 0, 0, 0];
    } else if (precentage >= 35 && precentage < 45) {
        starsLength = [1, 1, 0, 0, 0];
    } else if (precentage >= 45 && precentage < 55) {
        starsLength = [1, 1, .5, 0, 0];
    } else if (precentage >= 55 && precentage < 65) {
        starsLength = [1, 1, 1, 0, 0];
    } else if (precentage >= 65 && precentage < 75) {
        starsLength = [1, 1, 1, .5, 0];
    } else if (precentage >= 75 && precentage < 85) {
        starsLength = [1, 1, 1, 1, 0];
    } else if (precentage >= 85 && precentage < 95) {
        starsLength = [1, 1, 1, 1, .5];
    } else {
        starsLength = [1, 1, 1, 1, 1]; // 5 - full stars, 0 - half stars, 0 empty stars
    } 

    const stars = starsLength.map((star, idx) => {
            switch (star) {
                case 1:
                    return (
                        <svg key={idx} className="rating__star">
                            <use xlinkHref={`${Icons}#icon-star-full`}/>
                        </svg>
                    );
                case .5:
                    return (
                        <svg key={idx} className="rating__star">
                            <use xlinkHref={`${Icons}#icon-star-half`}/>
                        </svg>
                    );     
                default:
                    return (
                        <svg key={idx} className="rating__star">
                            <use xlinkHref={`${Icons}#icon-star-empty`}/>
                        </svg>
                    );  
            }      
    });

    return (
        <figure className="rating u-margin-bottom-low">
            {stars}
        </figure>
    );
};

export default itemRating;