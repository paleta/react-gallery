import React from 'react';
import './Logo.css';
import Logo from '../../assets/images/logo.png';

const logo = (props) => {

    let classes = ['logo-container'];
    classes.push(props.classes);

    return (
        <div className={classes.join(' ')}>
            <img src={Logo} className="logo" alt="logo" title="logo"/>
        </div>
    );
};

export default logo;