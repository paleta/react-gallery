import React from 'react';
import './Loader.css';
const loader = (props) => {

    let loaderClasses = ['lds-heart'];
    loaderClasses.push(props.loaderClasses);

    let loaderContainerClasses = ['loader-container'];
    loaderContainerClasses.push(props.loaderContainerClasses);

    return (
        <div className={loaderContainerClasses.join(' ')}>
            <div className={loaderClasses.join(' ')}>
                <div></div>
            </div>
        </div>
    );
};
export default loader;