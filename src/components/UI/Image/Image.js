import React from 'react';

import Loader from '../Loader/Loader';
import './Image.css';
import NotFound from '../NotFound/NotFound';

class Image extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};

        this.prom.then(() => {
            if (this.executeSetState) {
                this.setState({ready: true, error: false})
            }           
        })
        .catch(() => {
            if (this.executeSetState) {
                this.setState({ready: true, error: true})
            }
        });
    }

    executeSetState = true; 

    prom = new Promise((resolve, reject) => {
        const img = document.createElement('img');
        img.src = this.props.src;
        img.onload = () => {         
            resolve(true);
        };
        img.onerror = () => {
            reject(false);
        };
    });

    componentWillUnmount() {
        this.executeSetState = false;
    }

    render () {

        const classes = ['image'];
        classes.push(this.props.classes);

        const contentToRender = (this.state.ready && !this.state.error) ? (
            <img 
                className={classes.join(' ')}
                src={this.props.src}   
                title={this.props.title}
                alt={this.props.alt}         
            />

        ) : <Loader
                loaderContainerClasses="image__loader-container"
                loaderClasses="image__loader"/>;

        if (this.state.error) {
            return <NotFound>This picture seems broken ;(</NotFound>
        }

        return contentToRender;
    };
};
export default Image;