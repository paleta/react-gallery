import React from 'react';
import './Modal.css';
import Aux from '../../../hoc/_Aux/_Aux';
import Backdrop from '../Backdrop/Backdrop';
import Icons from '../../../assets/icons/sprite.svg';
import Button from '../Button/Button';

const modal = (props) => {

    const icon = props.icon ? <svg className="modal__icon"><use xlinkHref={`${Icons}#icon-question`} /></svg> : null;

    return (
        <Aux>
            <Backdrop clicked={props.onDecline}/>
            <div className="modal">
                <div className="modal__header">
                    {icon}
                    <h3 className="modal__title">{props.title}</h3>
                </div>
                <div className="modal__body"> 
                    {props.children}
                </div>
                <div className="modal__controls">
                    <Button 
                        type="button" 
                        classes="modal__btn"
                        clicked={props.onConfirm}>Yes</Button>
                    <Button 
                        type="button" 
                        classes="modal__btn"
                        clicked={props.onDecline}>No</Button>
                </div>
            </div>
        </Aux>
    );
};
export default modal;