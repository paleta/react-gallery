import React from 'react';
import './Hamburger.css';
const hamburger = (props) => {

    let hamburgerClasses = ['hamburger'];
    !!props.hamburgerClasses ? hamburgerClasses.push(props.hamburgerClasses) : null;
    let hamburgerIconClasses = ['hamburger__icon'];
    !!props.hamburgerIconClasses ? hamburgerIconClasses.push(props.hamburgerIconClasses) : null;

    props.open ? hamburgerIconClasses.push('open') : null;

    return (
        <div 
            onClick={props.clicked}
            className={hamburgerClasses.join(' ')}>
            <div className={hamburgerIconClasses.join(' ')}>

            </div>
        </div>
    );
};
export default hamburger;