import React from 'react';
import './Backdrop.css';
const component = (props) => {

    let classes = ['backdrop'];
    classes.push(props.classes);

    return (
        <div className={classes.join(' ')} onClick={props.clicked}></div>
    );
};
export default component;