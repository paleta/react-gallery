import React from 'react';
import './Snackbar.css';
import Icons from '../../../assets/icons/sprite.svg';


const snackbar = (props) => {

    let snackbar = (
        <div className="snackbar">
            <svg className="snackbar__icon">
                <use xlinkHref={`${Icons}#icon-bug`}/>
            </svg>
            <p className="snackbar__content">{props.children}</p>
        </div>
    );

    return snackbar;
};
export default snackbar;