import React from 'react';
import './Input.css';
const input = (props) => {

    const inputClasses = ['input__element'];
    inputClasses.push(props.classes);

    const labelClasses = ['input__label'];

    if (props.touched && props.invalid) {
        inputClasses.push('invalid');
        labelClasses.push('invalid');
    }

    let input = null

    if (props.type === 'textarea') {
        input = (
            <textarea

                name={props.name}
                id={props.name}
                rows={3}
                style={{resize: 'none'}}
                className={inputClasses.join(' ')}
                placeholder={props.placeholder} 
                onChange={props.changed} 
                onBlur={props.blured}
                value={props.value}>  
            </textarea>
        );
    } else {
        input = (
            <input             
                autoComplete={props.name}
                type={props.type} 
                name={props.name}
                id={props.name}
                className={inputClasses.join(' ')}
                placeholder={props.placeholder} 
                onChange={props.changed} 
                onBlur={props.blured}
                value={props.value}/> 
        );
    }

    return (
        <div className="input">
            {input}
            <label className={labelClasses.join(' ')} htmlFor={props.name}>{props.placeholder}</label>
        </div>
    );
};
export default input;