import React from 'react';

import './Button.css';

const button = (props) => {


    let classes = ['button'];
    classes.push(props.classes);
    
    return (
    
        <button
        className={classes.join(' ')}
        disabled={props.disabled}
        type={props.type}
        onClick={props.clicked}>
            {props.children}
        </button>
    );
};

export default button;