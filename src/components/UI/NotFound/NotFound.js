import React from 'react';
import './NotFound.css';
import Icons from '../../../assets/icons/sprite.svg';

const notFound = (props) => {

    return (
        <div className="not-found">
            <svg className="not-found__icon">
                <use xlinkHref={`${Icons}#icon-happy2`}/>
            </svg>
            <p className="not-found__text">
                {props.children} 
            </p>
        </div>
    );
};
export default notFound;