import React from 'react';
import './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';
import Aux from '../../hoc/_Aux/_Aux';

const navigationItems = (props) => {

    let classes = ['navigation'];
    classes.push(props.classes);

    let listClasses = ['navigation__list'];
    listClasses.push(props.listClasses);


    let naviagtion = (
        <Aux>
            <NavigationItem link="/" exact>Home</NavigationItem>
            <NavigationItem link="/gallery">Gallery</NavigationItem>
            <NavigationItem link="/sign-up">Sign up</NavigationItem>
            <NavigationItem link="/sign-in">Sign in</NavigationItem>
        </Aux>
    );
    if (props.isAuthenticated) {
        naviagtion = (
            <Aux>
                <NavigationItem link="/" exact>Home</NavigationItem>
                <NavigationItem link="/gallery">Gallery</NavigationItem>    
                <NavigationItem link="/logout">Logout</NavigationItem>        
            </Aux>
        );
    } 

    return (
        <nav className={classes.join(' ')}>
           <ul onClick={props.clicked} className={listClasses.join(' ')}>
                {naviagtion}
           </ul>
        </nav>
    );
};

export default navigationItems;