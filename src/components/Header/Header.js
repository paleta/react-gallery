import React from 'react';
import { Link } from 'react-router-dom';

import './Header.css';
import Logo from '../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Panel from '../Panel/Panel';
import Hamburger from '../UI/Hamburger/Hamburger';
import Backdrop from '../UI/Backdrop/Backdrop';

const header = (props) => {


    let userName = null;
    if (props.isAuthenticated) {
        userName = (
            <div className="header__user-name">
                Hello, {props.userName}!
            </div>
        );
    }

    let classes = ['header__nav'];
    props.showMobileMenu ? classes.push('open') : null;

    return (
        <header className="header">

            <div className="header__content">
                <Link to="/">
                    <Logo/>
                </Link>
                {userName}
                <div className="fill-remaning-space"></div>
                <NavigationItems classes={classes.join(' ')}
                                 clicked={props.onTriggerMobileMenu}
                                 listClasses="header__nav-list"
                                 isAuthenticated={props.isAuthenticated}/>
                <Hamburger 
                    open={props.showMobileMenu}
                    clicked={props.onTriggerMobileMenu}
                    hamburgerClasses="header__hamburger"/>
                {props.showMobileMenu ? <Backdrop 
                    clicked={props.onTriggerMobileMenu}
                    classes="header__backdrop"/> : null}
            </div>
            <Panel>{props.location}</Panel>

        </header>
    );
};

export default header;