import * as actionTypes from '../actionTypes';
import { sign, db } from '../../axios';
import { errorFormater } from '../../shared/utilities';
import { API_KEY } from '../../config';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token, userId, userName) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        userId: userId,
        userName: userName
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const authClearError = () => {
    return {
        type: actionTypes.AUTH_CLEAR_ERROR
    };
};

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    localStorage.removeItem('userName');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000);
    };
};


export const auth = (email, password, mode, userName) => {
    return dispatch => {
        dispatch(authStart());

        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        };

        let url = 'verifyPassword?key=' + API_KEY;
        if (mode === 'SIGN_UP') {
            url = 'signupNewUser?key=' + API_KEY;
        }  

        let user = null;

        sign.post(url, authData)
            .then(response => {
                const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);
                localStorage.setItem('token', response.data.idToken);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', response.data.localId);               
                dispatch(checkAuthTimeout(response.data.expiresIn));

                user = {
                    userId: response.data.localId,
                    token: response.data.idToken
                };

                if (mode === 'SIGN_UP') {
                    
                    return db.post(`users.json?auth=${response.data.idToken}`, {
                        id: response.data.localId,
                        name: userName
                    }); 

                } else {
                    return db.get(`users.json?orderBy="id"&equalTo="${response.data.localId}"&print=pretty`);                   
                }
                
            })
            .then((response => {
                if ( mode === 'SIGN_UP' ) {
                    localStorage.setItem('userName', userName);
                    dispatch(authSuccess(user.token, user.userId, userName));
                } else {
                    let name = null;
                    for (const key in response.data) {
                        if (response.data.hasOwnProperty(key)) {
                            name = response.data[key].name;                
                        }
                    }        
                    // console.log(name);                           
                    localStorage.setItem('userName', name);
                    dispatch(authSuccess(user.token, user.userId, name));
                }           
            }))
            .catch(err => {
                dispatch(authFail(errorFormater(err)));        
            });
               
    };
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                const userId = localStorage.getItem('userId');
                const userName = localStorage.getItem('userName');
                dispatch(authSuccess(token, userId, userName));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000 ));
            }   
        }
    };
};