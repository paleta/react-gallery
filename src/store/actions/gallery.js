import * as actionTypes from '../actionTypes';
import { db } from '../../axios';
import { errorFormater } from '../../shared/utilities';


//get

export const galleryGetItemsStart = () => {
    return {
        type: actionTypes.GALLERY_GET_ITEMS_START
    };
}; 

export const galleryGetItemsSuccess = galleryItems => {
    return {
        type: actionTypes.GALLERY_GET_ITEMS_SUCCESS,
        galleryItems: galleryItems
    };
};

export const galleryGetItemsFail = error => {
    return {
        type: actionTypes.GALLERY_GET_ITEMS_FAIL,
        error: error
    };
};

export const galleryGetItems = () => {
    return async dispatch => {
        dispatch(galleryGetItemsStart());
        try {
            const response = await db.get('galleryItems.json');
            
            const galleryItemsArray = [];
            for (const key in response.data) {
                if (response.data.hasOwnProperty(key)) {
                    galleryItemsArray.unshift({
                        ...response.data[key],
                        id: key
                    });            
                }
            }
            // console.log(galleryItemsArray);
            dispatch(galleryGetItemsSuccess(galleryItemsArray));
        } catch (err) {
            dispatch(galleryGetItemsFail(errorFormater(err)));
        }
        
    };
}; 

//add

export const galleryAddItemStart = () => {
    return {
        type: actionTypes.GALLERY_ADD_ITEM_START
    };
}; 

export const galleryAddItemSuccess = newItem => {
    return {
        type: actionTypes.GALLERY_ADD_ITEM_SUCCESS,
        newItem: newItem
    };
};

export const galleryAddItemFail = error => {
    return {
        type: actionTypes.GALLERY_ADD_ITEM_FAIL,
        error: error
    };
};

export const galleryAddItem = (newItem, token) => {
    return async dispatch => {
        dispatch(galleryAddItemStart());
        try {
            const response = await db.post(`galleryItems.json?auth=${token}`, newItem);
            newItem.id = response.data.name;   
            newItem.isNew = true;    
            dispatch(galleryAddItemSuccess(newItem));
        } catch (err) {
            dispatch(galleryAddItemFail(errorFormater(err)));  
        }
    };
};

export const galleryClearError = () => {
    return {
        type: actionTypes.GALLERY_CLEAR_ERROR
    };
};

//delete

export const galleryDeleteItemStart = () => {
    return {
        type: actionTypes.GALLERY_DELETE_ITEM_START
    };
}; 

export const galleryDeleteItemSuccess = itemIndex => {
    return {
        type: actionTypes.GALLERY_DELETE_ITEM_SUCCESS,
        itemIndex: itemIndex
    };
};

export const galleryDeleteItemFail = error => {
    return {
        type: actionTypes.GALLERY_DELETE_ITEM_FAIL,
        error: error
    };
};

export const galleryDeleteItem = (token, itemId, itemIndex) => {
    return async dispatch => {
        dispatch(galleryDeleteItemStart());
        try {
            await db.delete(`galleryItems/${itemId}.json?auth=${token}`);
            dispatch(galleryDeleteItemSuccess(itemIndex));
        } catch (err) {
            dispatch(galleryDeleteItemFail(errorFormater(err)));  
        }
    }
};

//update (vote)

export const galleryUpdateItemStart = () => {
    return {
        type: actionTypes.GALLERY_UPDATE_ITEM_START
    };
}; 

export const galleryUpdateItemSuccess = (updatedItem, itemIndex) => {
    return {
        type: actionTypes.GALLERY_UPDATE_ITEM_SUCCESS,
        updatedItem: updatedItem,
        itemIndex: itemIndex
    };
};

export const galleryUpdateItemFail = error => {
    return {
        type: actionTypes.GALLERY_UPDATE_ITEM_FAIL,
        error: error
    };
};

export const galleryUpdateItem = (token, updatedItem, itemIndex) => {
    return async dispatch => {
        dispatch(galleryUpdateItemStart());
        try {
            await db.put(`galleryItems/${updatedItem.id}.json?auth=${token}`, updatedItem);
            dispatch(galleryUpdateItemSuccess(updatedItem, itemIndex));
        } catch (err) {
            dispatch(galleryUpdateItemFail(errorFormater(err)));  
        }
    }
};

//get one

export const galleryGetItemStart = () => {
    return {
        type: actionTypes.GALLERY_GET_ITEM_START
    };
}; 

export const galleryGetItemSuccess = (galleryItem) => {
    return {
        type: actionTypes.GALLERY_GET_ITEM_SUCCESS,
        galleryItem: galleryItem,
    };
};

export const galleryGetItemFail = error => {
    return {
        type: actionTypes.GALLERY_GET_ITEM_FAIL,
        error: error
    };
};

export const galleryGetItem = (galleryItem, galleryItemId) => {
    return async dispatch => {
        dispatch(galleryGetItemStart());

        if (!!galleryItem) {
            dispatch(galleryGetItemSuccess(galleryItem));
        } else {

            try {
                const response = await db.get(`galleryItems/${galleryItemId}.json`);
                // console.log(response);
                dispatch(galleryGetItemSuccess(response.data));
            } catch (err) {
                dispatch(galleryGetItemFail(errorFormater(err)));  
            }

        }
    }
};