import * as actionTypes from '../actionTypes';

const initialState = {
    userId: null,
    token: null,
    userName: null,
    busy: false,
    error: null,
};

const authStart = (state) => {
    return {
        ...state,
        error: null,
        busy: true
    };
};

const authSuccess = (state, action) => {
    return {
        ...state,
        error: null,
        busy: false,
        token: action.token,
        userId: action.userId,
        userName: action.userName
    };
};

const authFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        busy: false
    };
};

const authLogout = (state) => {
    return {
        ...state,
        userId: null,
        token: null,
        userName: null,
    };
};

const authClearError = (state) => {
    return {
        ...state,
        error: null
    };
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START: return authStart(state);  
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_LOGOUT: return authLogout(state);  
        case actionTypes.AUTH_FAIL: return authFail(state, action);  
        case actionTypes.AUTH_CLEAR_ERROR: return authClearError(state);        
        default: return state;
    }
};

export default reducer;