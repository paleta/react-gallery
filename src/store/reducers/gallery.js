import * as actionTypes from '../actionTypes';

const initialState = {
    isBusyWithUpdate: false,
    isBusyWithAdd: false,
    isBusyWithGetAll: false,
    isBusyWithDelete: false,
    isBusyWithGetOne: false,
    galleryItem: null,
    galleryItems: null,
    error: null
};

const galleryGetItemsStart = (state) => {
    return {
        ...state,
        isBusyWithGetAll: true,
        error: null
    };
};

const galleryGetItemsSuccess = (state, action) => {
    return {
        ...state,
        isBusyWithGetAll: false,
        galleryItems: [
            ...action.galleryItems
        ],
        error: null
    };
};

const galleryGetItemsFail = (state, action) => {
    return {
        ...state,
        isBusyWithGetAll: false,
        error: action.error
    };
};

const galleryUpdateItemStart = (state) => {
    return {
        ...state,
        isBusyWithUpdate: true,
        error: null
    };
};

const galleryUpdateItemSuccess = (state, action) => {

    const galleryItems = [
        ...state.galleryItems,
    ];
    galleryItems[action.itemIndex] = {
        ...action.updatedItem
    };

    return {
        ...state,
        isBusyWithUpdate: false,
        galleryItems: [
            ...galleryItems
        ],
        error: null
    };
};

const galleryUpdateItemFail = (state, action) => {
    return {
        ...state,
        isBusyWithUpdate: false,
        error: action.error
    };
};

const galleryDeleteItemStart = (state) => {
    return {
        ...state,
        isBusyWithDelete: true,
        error: null
    };
};

const galleryDeleteItemSuccess = (state, action) => {

    const galleryItems = [
        ...state.galleryItems
    ];
    galleryItems.splice(action.itemIndex, 1);
    
    return {
        ...state,
        isBusyWithDelete: false,
        galleryItems: [
            ...galleryItems
        ],
        error: null
    };
};

const galleryDeleteItemFail = (state, action) => {
    return {
        ...state,
        isBusyWithDelete: false,
        error: action.error
    };
};

const galleryAddItemStart = (state) => {
    return {
        ...state,
        isBusyWithAdd: true,
        error: null
    };
};

const galleryAddItemSuccess = (state, action) => {

    const galleryItems = [
        ...state.galleryItems
    ];
    galleryItems.unshift(action.newItem);
    
    return {
        ...state,
        isBusyWithAdd: false,
        galleryItems: [
            ...galleryItems
        ],
        error: null
    };
};

const galleryAddItemFail = (state, action) => {
    return {
        ...state,
        isBusyWithAdd: false,
        error: action.error
    };
};

const galleryClearError = (state) => {
    return {
        ...state,
        error: null
    };
};

//single item

const galleryGetItemStart = (state) => {
    return {
        ...state,
        isBusyWithGetOne: true,
        error: null
    };
};

const galleryGetItemSuccess = (state, action) => {
    return {
        ...state,
        isBusyWithGetOne: false,
        galleryItem: {
            ...action.galleryItem
        },
        error: null
    };
};

const galleryGetItemFail = (state, action) => {
    return {
        ...state,
        isBusyWithGetOne: false,
        error: action.error
    };
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GALLERY_GET_ITEMS_START: return galleryGetItemsStart(state);
        case actionTypes.GALLERY_GET_ITEMS_SUCCESS: return galleryGetItemsSuccess(state, action);
        case actionTypes.GALLERY_GET_ITEMS_FAIL: return galleryGetItemsFail(state, action);

        case actionTypes.GALLERY_GET_ITEM_START: return galleryGetItemStart(state);
        case actionTypes.GALLERY_GET_ITEM_SUCCESS: return galleryGetItemSuccess(state, action);
        case actionTypes.GALLERY_GET_ITEM_FAIL: return galleryGetItemFail(state, action);

        case actionTypes.GALLERY_ADD_ITEM_START: return galleryAddItemStart(state);
        case actionTypes.GALLERY_ADD_ITEM_SUCCESS: return galleryAddItemSuccess(state, action);
        case actionTypes.GALLERY_ADD_ITEM_FAIL: return galleryAddItemFail(state, action);

        case actionTypes.GALLERY_UPDATE_ITEM_START: return galleryUpdateItemStart(state);
        case actionTypes.GALLERY_UPDATE_ITEM_SUCCESS: return galleryUpdateItemSuccess(state, action);
        case actionTypes.GALLERY_UPDATE_ITEM_FAIL: return galleryUpdateItemFail(state, action);

        case actionTypes.GALLERY_DELETE_ITEM_START: return galleryDeleteItemStart(state);
        case actionTypes.GALLERY_DELETE_ITEM_SUCCESS: return galleryDeleteItemSuccess(state, action);
        case actionTypes.GALLERY_DELETE_ITEM_FAIL: return galleryDeleteItemFail(state, action);

        case actionTypes.GALLERY_CLEAR_ERROR: return galleryClearError(state);
        default: return state;
    }
}

export default reducer;