import axios from 'axios';
import { DB_LINK } from './config';

export const sign = axios.create({
    baseURL: `https://www.googleapis.com/identitytoolkit/v3/relyingparty/`,
    timeout: 5000,
});

export const db = axios.create({
    baseURL: DB_LINK,
    timeout: 5000,
});